const plugins = {
	"autoprefixer": {},
	"postcss-preset-env": {
		features: {
			"nesting-rules": true,
			"custom-properties": true,
			"gray-function": true,
			"matches-pseudo-class": true,
			"not-pseudo-class": true,
			"media-query-ranges": true,
			"prefers-color-scheme-query": true,
		},
	},
};

if (process.env.NODE_ENV === "production") {
	const prod_plugins = {
		// plugins loaded only in production
		"cssnano": {
			preset: ["default", {
				cssDeclarationSorter: {},
				discardUnused: {},
				mergeIdents: {},
				reduceIdents: {},
			}],
		},
		"@fullhuman/postcss-purgecss": {
			// this is verbatim what is used in @fullhuman/vue-cli-plugin-purgecss:
			content: [ `./public/**/*.html`, `./src/**/*.vue` ],
			defaultExtractor (content) {
				const contentWithoutStyleBlocks = content.replace(/<style[^]+?<\/style>/gi, "");
				return contentWithoutStyleBlocks.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g) || [];
			},
			whitelist: [],
			whitelistPatterns: [ /-(leave|enter|appear)(|-(to|from|active))$/, /^(?!(|.*?:)cursor-move).+-move$/, /^router-link(|-exact)-active$/, /data-v-.*/ ],
		},
	};

	Object.assign(plugins, prod_plugins);
}

module.exports = {
	plugins,
};
