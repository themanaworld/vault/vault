/**
 * An identity of a Vault user
 * Currently this can only be an email
 */
export interface Identity {
	id: number;
	email: string;
	added: Date;
	primary: boolean;
}

/**
 * a game character
 */
export interface Char {
	/** the public name of the character */
	name: string;
	/** the ID of the character */
	charId: number;
	/** baseLevel */
	level: number;
	/** gender */
	sex: string; // FIXME: make this a const enum
}

/**
 * base game account
 */
interface Account {
	/** game login username */
	name: string;
	/** game account ID */
	accountId: number;
	/** characters associated with the account */
	chars: Char[];
}

/**
 * legacy game account
 */
export interface LegacyAccount extends Account {
	/** the ID of the target account on the new server */
	revoltId?: number;
}

/**
 * evol game account
 */
export interface GameAccount extends Account {
	/** the ID of the source account on the Legacy server */
	legacyId?: number;
}

/**
 * what authentication flow was used for logging in
 */
export const enum LoginMethod {
	/** logged in with a magic link sent by email */
	Email,
}

/**
 * an API session
 */
export interface Session {
	/** the method that was used to login (email, webauthn, ...) */
	loginMethod: LoginMethod;
	/** the UUID of the session */
	sessionKey: string;
	/** the secret key of the session  */
	secretKey: string;
	/** the current expiration of the session (gets rehydrated frequently) */
	expires$D: Date;
	/** the ID of the identity that was used to log in */
	identity: number;
}

/**
 * a login request
 */
export interface LoginAttempt {
	/** when the user initiated the login request */
	initiatedAt: Date;
	/** address that was used to initiate the request */
	email: string;
}

/**
 * Vault account data
 */
export interface AccountData {
	/** the main account identity */
	primaryIdentity: number;
	/** whether to allow to login with a non-primary identity */
	allowNonPrimary: boolean;
}

/**
 * all of the possible color schemes
 */
export const enum ColorScheme {
	/** Explicitly user-selected: light mode */
	Light,
	/** Explicitly user-selected: dark mode */
	Dark,
	/** OS settings: dark mode  */
	OSDark,
	/** OS settings: light mode  */
	OSLight,
	/** OS settings: no preference */
	NoPreference, // we use the default theme in this case
}

/**
 * app appearance settings
 */
export interface DisplayOptions {
	/** dark or light mode */
	colorScheme: ColorScheme;
}

// other types
export { default as REGEXES } from "./regexes"
