// commonly used regexes

export const REGEXES = Object.freeze({
	UUID: /^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/i, // 122 bits
	NANO: /^[A-Za-z0-9_-]{21,26}$/, // min 126 bits
})

export default REGEXES
