import Vue from "vue"
import Vuex from "vuex"
import createPersistedState from "vuex-persistedstate"
import { Identity, Session, GameAccount, LegacyAccount, LoginMethod, AccountData, LoginAttempt, ColorScheme, DisplayOptions } from "@/types"

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		session: {
			loginMethod: LoginMethod.Email,
			sessionKey: "", // key returned by the API
			secretKey: "", // secret key returned by the API
			expires$D: new Date(), // expiration of the key
			identity: 0, // identity id of the email that was used to log in
		} as Session,
		loginAttempt: {
			initiatedAt: new Date(0),
			email: "",
		} as LoginAttempt,
		checkedSession_: false, // temporary var: the session has been re-checked with the api
		identities: [] as Identity[],
		legacyAccounts: [] as LegacyAccount[],
		gameAccounts: [] as GameAccount[],
		account: {
			vaultId: 0,
			primaryIdentity: 0,
			allowNonPrimary: true,
			strictIPCheck: true,
		} as AccountData,
		displayOptions: {
			colorScheme: ColorScheme.NoPreference,
		} as DisplayOptions,
	},
	getters: {
		isLoggedIn: state => state.session.sessionKey !== "",
		isExpired: state => () => state.session.expires$D <= new Date(),
		timeLeft: state => () => {
			if (state.session.expires$D === null)
				return 0

			return Math.ceil(state.session.expires$D.getTime() - (new Date()).getTime())
		},
		checkedSession: state => state.checkedSession_,
		/**
		 * check whether the cached login attempt is still valid
		 */
		loginAttemptValid: state => () => {
			if (state.loginAttempt.email !== "") {
				const now = new Date()
				const diff = now.getTime() - state.loginAttempt.initiatedAt.getTime()
				const minutes = Math.ceil(((diff % 86400000) % 3600000) / 60000)

				if (minutes >= 30) {
					/** login request is expired */
					return false
				} else {
					/** login request is still valid */
					return true
				}
			} else {
				/** no cached login request */
				return false
			}
		},
		darkMode: state => state.displayOptions.colorScheme === ColorScheme.Dark ||
			state.displayOptions.colorScheme === ColorScheme.OSDark,
	},
	mutations: {
		session (state, newSession) {
			state.session.sessionKey = newSession.key ?? ""
			state.session.identity = newSession.identity ?? 0

			if (Reflect.has(newSession, "expires"))
				state.session.expires$D = new Date(newSession.expires)

			if (Reflect.has(newSession, "secret"))
				state.session.secretKey = newSession.secret
		},
		checkedSession (state) {
			state.checkedSession_ = true
		},
		setLoginAttempt (state, email: string) {
			state.loginAttempt.email = email
			state.loginAttempt.initiatedAt = new Date()
		},
		expiry (state, expiryDate) {
			state.session.expires$D = new Date(expiryDate)
		},
		setIdentities (state, newIdent) {
			state.identities = newIdent.slice()
		},
		pushIdentity (state, newIdent) {
			state.identities.push(Object.assign({}, newIdent))
		},
		setAccountData (state, newData) {
			Object.assign(state.account, newData)

			// TODO: deprecate session.primary and only use account.primaryIdentity
			for (const ident of state.identities) {
				if (ident.id === newData.primaryIdentity) {
					ident.primary = true
				} else if (ident.primary) {
					ident.primary = false
				}
			}
		},
		setLegacyAccounts (state, newAccs) {
			state.legacyAccounts = newAccs.slice()
		},
		pushLegacyAccount (state, newAcc) {
			state.legacyAccounts.push(Object.assign({}, newAcc))
		},
		setGameAccounts (state, newAccs) {
			state.gameAccounts = newAccs.slice()
		},
		pushGameAccount (state, newAcc) {
			state.gameAccounts.push(Object.assign({}, newAcc))
		},
		setLegacyRevoltId (state, params) {
			for (const acc of state.legacyAccounts) {
				if (acc.accountId === params.legacy) {
					acc.revoltId = params.revolt
					break
				}
			}
		},
		setLegacyAccount (state, newAcc) {
			for (const acc of state.legacyAccounts) {
				if (acc.accountId === newAcc.accountId) {
					Object.assign(acc, newAcc)
					break
				}
			}
		},
		setGameAccount (state, newAcc) {
			for (const acc of state.gameAccounts) {
				if (acc.accountId === newAcc.accountId) {
					Object.assign(acc, newAcc)
					break
				}
			}
		},
		setColorScheme (state, scheme: ColorScheme) {
			state.displayOptions.colorScheme = scheme
		},
	},
	actions: {
	},
	modules: {
	},

	plugins: [
		createPersistedState({
			getState (key, storage) {
				try {
					const value = storage.getItem(key)
					if (typeof value !== "undefined") {
						const json = JSON.parse(value)
						json.checkedSession_ = false // always reset this check
						if (json?.session?.expires$D) {
							json.session.expires$D = new Date(json.session.expires$D)
						}
						if (json?.loginAttempt?.initiatedAt) {
							json.loginAttempt.initiatedAt = new Date(json.loginAttempt.initiatedAt)
						}
						return json
					}
				} catch (err) { return null }
				return null
			},
		}),

		/** use a BroadcastChannel to keep tabs in sync */
		store => {
			const bc = new BroadcastChannel("vault:vuex")
			let lastMsg = ""

			// listen to actions from others
			bc.addEventListener("message", async event => {
				const {type, payload} = event.data
				lastMsg = JSON.stringify(event.data)
				await store.commit(type, payload)
			})

			// broadcast our actions
			store.subscribe(async mutation => {
				const json = JSON.stringify(mutation)
				if (lastMsg !== json) {
					lastMsg = json // log the last message to avoid replay
					await bc.postMessage(mutation)
				}
			})
		},
	],
})
