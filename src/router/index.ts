import Vue from "vue"
import VueRouter from "vue-router"
import Intro from "../views/Intro.vue"
import Login from "../views/Login.vue"

Vue.use(VueRouter)

const routes = [
	{
		path: "/",
		name: "intro",
		component: Intro,
		meta: {
			availableOffline: true,
		},
	},
	{
		path: "/login",
		name: "login",
		component: Login,
		meta: {
			title: "Log In",
		},
	},
	{
		path: "/auth/:key",
		name: "authentication",
		component: () => import(/* webpackChunkName: "vault" */ "../views/Auth.vue"),
		meta: {
			title: "Log In",
		},
	},
	{
		path: "/validate/:key",
		name: "validation",
		component: () => import(/* webpackChunkName: "emailvalidate" */ "../views/EmailValidate.vue"),
		meta: {
			title: "Email Validation",
		},
	},
	{
		path: "/vault",
		component: () => import(/* webpackChunkName: "vault" */ "../views/Vault.vue") ,
		meta: {
			requiresAuth: true,
			title: "My Vault",
		},
		children: [
			{
				path: "",
				name: "vault",
				component: () => import(/* webpackChunkName: "vault" */ "../views/Vault__Overview.vue"),
				meta: {
					title: "Overview",
				},

			},
			{
				path: "manage",
				name: "settings",
				component: () => import(/* webpackChunkName: "vault" */ "../views/Vault__Manage.vue"),
				meta: {
					title: "Account Management",
				},
			},
		],
	},
	{
		path: "/503",
		name: "net-error",
		component: () => import(/* webpackChunkName: "net-error" */ "../views/NetworkError.vue"),
		meta: {
			title: "Error",
			availableOffline: true,
		},
	},
	{
		path: "/offline",
		name: "offline",
		component: () => import(/* webpackChunkName: "net-error" */ "../views/NetworkOffline.vue"),
		meta: {
			title: "Offline",
			availableOffline: true,
		},
	},
	{
		path: "*",
		name: "404",
		component: () => import(/* webpackChunkName: "net-error" */ "../views/NotFound.vue"),
		meta: {
			title: "Not Found",
			availableOffline: true,
		},
	},
]

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes,
})

router.beforeEach((to, from, next) => {
	const isOffline = !navigator.onLine

	if (isOffline && to.name !== "offline" && !to.meta?.availableOffline) {
		next({name: "offline"})
	} else {
		next()
	}
})

// we could also have a beforeEach to display a spinner
router.beforeResolve((to, from, next) => {
	self.dispatchEvent(new Event("loaded"))
	next()
})


// getting original metadata
const baseTitle = document.title

// updating page metadata
router.afterEach(to => {
	const route = to.meta && to.meta.title ? to :
		to.matched.slice().reverse().find(r => r.meta && r.meta.title)

	if (route && route.meta.title) {
		const newTitle = route.meta.title
		document.title = `${baseTitle} — ${newTitle}`
	} else {
		document.title = baseTitle
	}
})

export default router
