const console_config = ["error", { allow: ["warn", "error"] }];

module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: [
		"plugin:vue/essential",
		"eslint:recommended",
		"@vue/typescript/recommended",
	],
	parserOptions: {
		ecmaVersion: 2020,
	},
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? console_config : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
		"indent": ["error", "tab"],
		"quotes": ["error", "double", { allowTemplateLiterals: true }],
		"semi": ["error", "never"],
		"quote-props": ["error", "consistent-as-needed"],
		"comma-dangle": ["error", "always-multiline"],
		"space-before-function-paren": ["error", "always"],
	},
}
