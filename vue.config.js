/* eslint-disable @typescript-eslint/no-var-requires */
const CompressionPlugin = require("compression-webpack-plugin")

module.exports = {
	integrity: true, // enable SRI in script/style tags
	parallel: true,
	configureWebpack: {
		plugins: process.env.NODE_ENV === "production" ? [
			new CompressionPlugin({
				filename: "[path].br[query]",
				algorithm: "brotliCompress",
				test: /\.(js|css|html|svg|ico|png|webp|ttf|woff|woff2)$/,
				compressionOptions: { level: 11 },
				minRatio: 0.9,
			}),
		]: [],
	},
	pwa: {
		workboxPluginMode: "GenerateSW",
		workboxOptions: {},
		name: "TMW Vault",
		themeColor: "#145216",
		manifestPath: "manifest.webmanifest",
		manifestOptions: {
			description: "Account manager for The Mana World",
			lang: "en-US",
			start_url: "/vault",
			scope: "/",
			display: "standalone",
			background_color: "#F4F4F1",
			// TODO: 192x192, 512x512 icons
		},
	},
}
